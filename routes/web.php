<?php

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('html', function () {
    return view('html');
})->name('html');

Route::get('javascript', function () {
    return view('javascript');
})->name('javascript');

Route::get('dica-css', function () {
    return view('dicacss');
})->name('dica-css');

Route::get('videoaulas', function () {
    return view('videoaulas');
})->name('videoaulas');

Route::get('contato', function () {
    return view('contato');
})->name('contato');

