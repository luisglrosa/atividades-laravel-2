@extends('layout.site')
@section('conteudo')
    <div class="row">
        <div class="col-6">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/iZ1ucWosOww" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="col-6">
            <h1>Video Aulas</h1>
            <h2>Curso de HTML e CSS para iniciantes Aula 1</h2>
            <p>Nesta primeira aula vou passar
                o conceito básico de HTML
                e mostrar
                como criar
                s ua primeira página para teste.
                Com esta página você vai
                aprender
                a criar
                a estrutur a mais
                simples
                de um documento HTML,
                definir
                títulos
                e também a codificação ( char set )
                para seus
                documentos </p>
        </div>
                <div class="row">
                    <div class="col-4">
                        <iframe width="350" height="200" src="https://www.youtube.com/embed/ze9--J4PJ_4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <h3>GitHub, GitLab ou Bitbucket? Qual nós usamos! // CAC #6</h3>
                    </div>
                    <div class="col-4">
                        <iframe width="350" height="200" src="https://www.youtube.com/embed/jyTNhT67ZyY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <h3>MVC // Dicionário do Programador</h3>
                    </div>
                    <div class="col-4">
                        <iframe width="350" height="200" src="https://www.youtube.com/embed/5K7OGSsWlzU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <h3>O que um Analista de Sistemas faz? // Vlog #77</h3>
                    </div>
                </div>

        </div>
    </div>






@endsection