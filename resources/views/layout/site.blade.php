<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">

    <title>HTML</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
        integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="css/menu.css">
</head>

<body>
    <div class="container">
        <div class="row">
            <header class="cabecalho col-12 text-center">
                <div class="logo">
                    <img src="img/logo.png" alt="img">
                </div>
                <nav id="menu">
                    <ul>
                    <li><a href="{{route('home')}}"> HOME</a></li>
                        <li><a href="{{route('html')}}">HTML</a></li>
                        <li><a href="{{route('javascript')}}"> JAVASCRIPT</a></li>
                        <li><a href="{{route('dica-css')}}">CSS</a></li>
                        <li><a href="videoaulas"> VIDEO AULAS</a></li>
                        <li><a href="contato"> CONTATO</a></li>
                    </ul>
                </nav>
            </header>
        </div>
        <main>
         @yield('conteudo')
       </main>

       <footer>

           <div class="row">
               <div class="col-12">
               
           </div>
        </div>
       </footer>
       <p class="text-center">TODOS OS DI REITOS SÃO RESERVADOS</p>

    </div>
</body>

</html>
